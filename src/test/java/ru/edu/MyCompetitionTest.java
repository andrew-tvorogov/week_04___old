package ru.edu;

import org.junit.Test;
import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.MyAthlete;
import ru.edu.model.Participant;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class MyCompetitionTest {

    //Competition competition = mock(Competition.class); // реализация
    MyCompetition competition = new MyCompetition();
    Athlete first = new MyAthlete("Alex", "Smirnov", "Russia");
    Athlete firstSame = new MyAthlete("Alex", "Smirnov", "Russia");
    Athlete firstSameName = new MyAthlete("Alex", "Timofeev", "Russia");
    Athlete firstSameNameLong = new MyAthlete("Alex", "Timofeevsky", "Russia");
    Athlete second = new MyAthlete("Ivan", "Ivanov", "Belarus");
    Athlete third = new MyAthlete("Max", "Sidorov", "Ukraine");

    @Test
    public void registerSameContent() {
        competition.register(first);
        try {
            competition.register(firstSame);
            fail("Need to prevent duplicate registration"); // подсказака, если не обработаны дубликаты
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void registerSameObject() {
        competition.register(first);
        try {
            competition.register(first);
            fail("Need to prevent duplicate registration"); // подсказака, если не обработаны дубликаты
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }

    @Test
    public void updateScore() {
        competition.register(first);
        competition.updateScore(1, 10);
        List<Participant> results = competition.getResults();
        Participant participant = competition.getParticipantById(1);
        assertEquals(10, participant.getScore());

        competition.register(second);
        competition.updateScore(2, 15);
        competition.updateScore(1, 30);
        competition.register(third);
        competition.updateScore(3, 100);
        Participant participant2 = competition.getParticipantById(2);
    }

    @Test
    public void updateScoreByParticipant() {
        competition.register(first);
        List<Participant> results = competition.getResults();
        Participant participant = competition.getParticipantById(1);
        competition.updateScore(participant, 15);
        assertEquals(15, participant.getScore());

        competition.updateScore(participant, 10);
        assertEquals(25, participant.getScore());
    }

    @Test
    public void getResults() {
        competition.register(first);
        competition.updateScore(1, 10);
        competition.updateScore(1, -2);
        List<Participant> results = competition.getResults();
        assertEquals("[{id=1, score=8, athlete= {firstName='Alex', lastName='Smirnov', country='Russia'}}]", results.toString());

        competition.register(second);
        competition.register(third);
        competition.updateScore(2, 10);
        competition.updateScore(3, 15);
        competition.updateScore(1, 7);
        competition.updateScore(3, -1);
        results = competition.getResults();
        assertEquals(3, results.size());
    }

    @Test
    public void getParticipantsCountries() {
        competition.register(first);
        competition.updateScore(1, 10);
        competition.register(second);
        competition.updateScore(2, 15);
        competition.updateScore(1, 30);
        competition.register(third);
        competition.updateScore(3, 100);
        competition.register(firstSameNameLong);
        competition.updateScore(4, 10);
        List<CountryParticipant> countryParticipants = competition.getParticipantsCountries();
        String countries = countryParticipants.toString();
        System.out.println(countries);
    }

    @Test
    public void getParticipants() {
        competition.register(first);
        competition.register(firstSameName);
        competition.register(firstSameNameLong);
        String out = competition.getParticipants().toString();
        assertEquals("[ {firstName='Alex', lastName='Smirnov', country='Russia'},  {firstName='Alex', lastName='Timofeev', country='Russia'},  {firstName='Alex', lastName='Timofeevsky', country='Russia'}]", out);
    }

    @Test
    public void getParticipantById() {
        competition.register(first);
        Participant participant = competition.getParticipantById(1);
        assertEquals("{id=1, score=0, athlete= {firstName='Alex', lastName='Smirnov', country='Russia'}}", participant.toString());
    }
}
