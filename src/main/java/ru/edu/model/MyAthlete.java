package ru.edu.model;

import java.util.Objects;

public class MyAthlete implements Athlete {
    private String firstName;
    private String lastName;
    private String country;

    public MyAthlete(String first, String last, String countryName) {
        this.firstName = first;
        this.lastName = last;
        this.country = countryName;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return " {" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyAthlete myAthlete = (MyAthlete) o;
        return Objects.equals(firstName, myAthlete.firstName)
                && Objects.equals(lastName, myAthlete.lastName)
                && Objects.equals(country, myAthlete.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }
}
