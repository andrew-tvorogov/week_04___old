package ru.edu.model;

import java.util.LinkedList;
import java.util.List;

public class MyCountryParticipant implements CountryParticipant {
    private String countryName;
    private long countryScore;
    private List<Participant> countryParticipants = new LinkedList<>();

    public MyCountryParticipant(String name) {
        this.countryName = name;
    }

    public void incrementScore(long value) {
        countryScore += value;
    }

    public void addParticipant(Participant participant) {
        countryParticipants.add(participant);
    }

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return countryName;
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return countryParticipants;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return countryScore;
    }

    @Override
    public String toString() {
        return "\nMyCountryParticipant{" +
                "countryName='" + countryName + '\'' +
                ", countryScore=" + countryScore +
                ", countryParticipants=" + countryParticipants +
                '}';
    }
}
