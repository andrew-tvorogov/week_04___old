package ru.edu;

import ru.edu.model.*;

import java.util.*;

public class MyCompetition implements Competition {
    private long id = 0;
    private Map<Long, MyParticipant> participantMap = new HashMap<>();
    private Set<Athlete> registeredAthletes = new HashSet<>();
    private Map<String, MyCountryParticipant> countryParticipantMap = new HashMap<>();

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        if (registeredAthletes.contains(participant)) {
            throw new IllegalArgumentException("Duplicate registration!");
        }
        registeredAthletes.add(participant);
        MyParticipant internalAndSecuredObject = new MyParticipant(++id, participant);
        participantMap.put(internalAndSecuredObject.id, internalAndSecuredObject);

        // в map попадает ключ - имя страны, значение - объект этой страны
        if (!countryParticipantMap.containsKey(participant.getCountry())) {
            countryParticipantMap.put(participant.getCountry(),
                    new MyCountryParticipant(participant.getCountry()));
        }

        MyCountryParticipant country = countryParticipantMap.get(participant.getCountry());
        country.addParticipant(internalAndSecuredObject);

        MyParticipant externalCopy = internalAndSecuredObject.getClone();
        return externalCopy;
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        MyParticipant participant = participantMap.get(id);
        if (participant != null) {
            participant.score += score;

            String countryName = participant.athlete.getCountry();
            MyCountryParticipant country = countryParticipantMap.get(countryName);
            country.incrementScore(score);
        }
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score); // используем готовый метод updateScore(id, score)
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        LinkedList<Participant> participants = new LinkedList<>(participantMap.values());
        participants.sort((a, b) -> {
            return (int) (b.getScore() - a.getScore());
        });
        return participants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        LinkedList<CountryParticipant> countryParticipants = new LinkedList<>(
                countryParticipantMap.values());
        countryParticipants.sort((a, b) -> {
            return (int) (b.getScore() - a.getScore());
        });
        return countryParticipants;
    }

    public List<Athlete> getParticipants() {
        int registeredSize = registeredAthletes.size();
        Athlete[] athletesArr = new Athlete[registeredSize];
        System.arraycopy(registeredAthletes.toArray(), 0, athletesArr, 0, registeredSize);
        List<Athlete> participants = new LinkedList<>(Arrays.asList(athletesArr));
        participants.sort((a, b) -> {
            String first = a.getFirstName() + a.getLastName();
            String second = b.getFirstName() + b.getLastName();
            return first.compareToIgnoreCase(second);
        });
        return participants;
    }

    public MyParticipant getParticipantById(long id) {
        return participantMap.get(id);
    }

    private static class MyParticipant implements Participant {
        public final Long id;
        private long score;
        private Athlete athlete;

        private MyParticipant(long id, Athlete athlete) {
            this.id = id;
            this.athlete = athlete;
        }

        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */
        @Override
        public Long getId() {
            return id;
        }

        /**
         * Информация о спортсмене
         *
         * @return объект спортсмена
         */
        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */
        @Override
        public long getScore() {
            return score;
        }

        public void incrementScore(long value) {
            score += value;
        }

        @Override
        public String toString() {
            return "{" +
                    "id=" + id +
                    ", score=" + score +
                    ", athlete=" + athlete +
                    '}';
        }

        public MyParticipant getClone() {
            Athlete athleteCloned = new MyAthlete(athlete.getFirstName(),
                    athlete.getLastName(), athlete.getCountry());
            return new MyParticipant(id, athleteCloned);
        }
    }
}
